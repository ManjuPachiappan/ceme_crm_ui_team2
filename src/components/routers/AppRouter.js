import React from 'react'
import { Route, BrowserRouter as Router } from 'react-router-dom'
import HomePage from '../home/HomePage'
import Login from '../login/Login'
import HeaderNav from '../navigation/navigation.header'
import SideNav from '../navigation/navigation.side'
import DashboardContainer from '../dashboard/DashboardContainer'
import CustomerContainer from '../customer/CustomerContainer'
import UserContainer  from '../User/UserContainer'
import PrivateRouter from './PrivateRouter'
import { connect } from 'react-redux'

const AppRouter = (props) => {
    return (
        <>
            <Router>
                <HeaderNav />
                <div className="row">
                    
                        <SideNav />
                        <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-md-4">
                            <Route exact path="/" component={HomePage} />
                            <Route exact path="/login" component={Login} />
                            <PrivateRouter exact path="/dashboard" component={DashboardContainer} {...props}/>
                            <PrivateRouter exact path="/customer" component={CustomerContainer} {...props}/>
                            <PrivateRouter exact path="/user" component={UserContainer} {...props}/>
                        
                        </main>
                </div>

            </Router>

        </>


    )
}

const mapStateToProps = (state) => {

    const { isLoggedIn } = state.auth;
    return {
        isLoggedIn,
    };
}

export default connect(mapStateToProps)(AppRouter);

