import React from 'react';
import {Route,Redirect} from 'react-router-dom';

const PrivateRouter= ({component:Component,isLoggedIn,...rest})=>(
    <Route {...rest}
    render={(props)=>(
        isLoggedIn===true?<Component {...props}/>:<Redirect to={{pathname:'/login', state:{from: props.location}}} />
    )} /> 
    );

export default PrivateRouter