import React from 'react'
import Chart from "react-google-charts";
import Loader from '../utilities/Loader'

class PieChart extends React.Component {

    render() {
        return (
            <Chart
                width={'700px'}
                height={'500px'}
                chartType="PieChart"
                loader={<Loader />}
                data={this.props.data}
                options={this.props.options}
            />
        )
    }
}

export default PieChart