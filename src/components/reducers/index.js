import {combineReducers} from 'redux'
import {loginReducer} from './LoginReducers'
import {interactionReducers} from './InteractionReducers'

const reducer = combineReducers({
   auth: loginReducer,
   interaction:interactionReducers

})


export default reducer