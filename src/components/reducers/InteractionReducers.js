import {INTERACTION_CONSTANTS as CONSTANTS} from '../constants/Constants'

const initialState = {
    interactions:[],
    message:"",
    loading:false
}

export const interactionReducers = (state =initialState,action) =>{
    console.log(action.type)
    switch(action.type)
    {       
        case CONSTANTS.INTERACTION_FETCH_BIGIN:
            return {...state, message:"",loading:true}
        case CONSTANTS.INTERACTION_FETCH_SUCCESS:
            return {...state,interactions:action.data, message:"",loading:false}
        case CONSTANTS.INTERACTION_FETCH_FAILURE:
            return {...state,interactions:[], message:action.message,loading:false}
        case CONSTANTS.INTERACTION_ADD_SUCCESS:
        case CONSTANTS.INTERACTION_UPDATE_SUCCESS:
        case CONSTANTS.INTERACTION_DELETE_SUCCESS:
            return {...state, message:""}
        case CONSTANTS.INTERACTION_DELETE_FAILURE:
        case CONSTANTS.INTERACTION_ADD_FAILURE:
        case CONSTANTS.INTERACTION_UPDATE_FAILURE:
            return {...state,message:action.message,loading:false}
        case CONSTANTS.INTERACTION_ERROR_RESET:
            return {...state,message:"",loading:false}
        default:
            return state
    }    

}