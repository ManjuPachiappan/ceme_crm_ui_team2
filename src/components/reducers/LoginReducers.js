import {LOGIN_CONSTANTS as CONSTANTS} from '../constants/Constants'

const initialState = {
    user:null,
    isLoggedIn:false,
    message:""
}

export const loginReducer = (state =initialState,action) =>{
    console.log(action.type)
    switch(action.type)
    {
       

        case CONSTANTS.LOGIN_SUCCESS:
            return {...state,user:action.data, isLoggedIn:true,message:""}
        case CONSTANTS.LOGIN_FAILURE:
            return {...state,user:null, isLoggedIn:false,message:action.message}
        case CONSTANTS.LOGOUT:
        case CONSTANTS.LOGIN_BEGIN:
            return {...state,user:null,isLoggedIn:false, message:""}
        default:
            return state
    }    
}