import React, { useState} from 'react'
import { useSelector, useDispatch  } from "react-redux";
import {login} from '../store/LoginActions'
import { useHistory } from 'react-router-dom';
import {  fetctInteractions} from "../store/InteractionActions";

const Login = () => {
    const [username, setUserName] = useState("")
    const [password, setPassword] = useState("")
    const [error, setError] = useState("")
    const auth = useSelector((state) => state.auth)
    const history = useHistory();
    const dispatch = useDispatch();
    

    const handleSubmit = (event) => {

        event.preventDefault();
        if (username && password)
        {
            dispatch(login({username,password})).then(
                (res) =>{ history.push("/dashboard")
                dispatch(fetctInteractions())
            },
                (err) => setError(auth.message)
            )
            
        }
        else
           setError("User Name and Password both Required")
    }
    
    const handleChange = (field,setValue) => {
        setValue(field.target.value) 
        error && setError("")  
             
    }

    

    return (
        <div className="container mt-4">
            <div className="col-md-5">
           
                <form className="form-signin" onSubmit={(e) => handleSubmit(e)}>
                    <div className="text-center mb-4">
                        <img className="mb-4" src="F:\CEME\ceme_crm_ui\src\logo.svg" alt="" width="72" height="72" />
                        <h1 className="h3 mb-3 font-weight-normal">Sign In</h1>
                    </div>

                    <div className="form-label-group">
                        <input type="text" id="username" value={username} onChange={(e)=>handleChange(e,setUserName)} className="form-control" placeholder="User Name" required="" autoFocus="" />
                        <label forhtml="username">User Name</label>
                    </div>

                    <div className="form-label-group">
                        <input type="password" id="password" value={password} className="form-control" onChange={(e)=>handleChange(e,setPassword)} placeholder="Password" required="" />
                        <label forhtml="password">Password</label>
                    </div>

                    <button className="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                    {error && <div className="alert alert-danger alert-dismissible fade show">
                            <strong>Error!</strong> {error} 
                    </div>}
                        
                    <p className="mt-5 mb-3 text-muted text-center">© allstate crm 2020</p>
                </form>
            </div>
        </div>

    )
}

export default Login
