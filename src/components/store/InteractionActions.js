import axios from 'axios'
import {INTERACTION_CONSTANTS as CONSTANTS} from '../constants/Constants'
import { getInteractionsAPI,addInteractionsAPI,deleteInteractionsAPI,updateInteractionsAPI } from "../Temp/Interactions";

const API_URL = "http://localhost:8080/api/interaction/";

export const fetctInteractions = () =>{
    return (dispatch,getState) =>{
       dispatch(fetchInteractionBegin())
     //  setTimeout(()=>( //remove
            getInteractionsAPI() //axios.get(API_URL + "all")
			.then(
                (response) => {
                    dispatch(fetchInteractionSuccess(response.data))
                },
                (error) => {
                    dispatch(fetchInteractionFailure())
                    throw error
                })
           // ),3000)  //remove
}}


export const updateInteractions = (data) =>{
    return (dispatch,getState) =>{
        dispatch(fetchInteractionBegin())
        return updateInteractionsAPI(data) //axios.get(API_URL + "all")
        .then(
                (response) => {
                    dispatch(updateInteractionSuccess())
                    dispatch(fetctInteractions())
                },
                (error) => {
                    dispatch(updateInteractionFailure())
                    throw error
                });
}}

export const addInteractions = (data) =>{
    return (dispatch,getState) =>{
        dispatch(fetchInteractionBegin())
        return addInteractionsAPI(data) //axios.get(API_URL + "all")
        .then(
                (response) => {
                    dispatch(addInteractionSuccess())
                    dispatch(fetctInteractions())
                },
                (error) => {
                    dispatch(addInteractionFailure())
                    throw error
                });
}}



export const deleteInteractions = (id) =>{
    
    return (dispatch,getState) =>{
        dispatch(fetchInteractionBegin())
        return deleteInteractionsAPI(id) //return axios.post(API_URL +  id)
			.then(
                (response) => {
                    dispatch(deleteInteractionSuccess())
                    dispatch(fetctInteractions())
                },
                (error) => {
                    dispatch(deleteInteractionFailure())
                    throw error
                });
}}

export const fetchInteractionBegin = () =>{
    return{
        type:CONSTANTS.INTERACTION_FETCH_BIGIN,
    }
}

export const fetchInteractionSuccess = (data) =>{
    return{
        type:CONSTANTS.INTERACTION_FETCH_SUCCESS,
        data:data
    }
}

export const fetchInteractionFailure = (err) =>{
    return{
        type:CONSTANTS.INTERACTION_FETCH_FAILURE,
        message:"Unable to fetch User Interactions"
    }
}

export const addInteractionSuccess = () =>{
    return{
        type:CONSTANTS.INTERACTION_ADD_SUCCESS,
    }
}

export const addInteractionFailure = (err) =>{
    return{
        type:CONSTANTS.INTERACTION_ADD_FAILURE,
        message:"Unable to Add User Interactions"
    }
}

export const updateInteractionSuccess = () =>{
    return{
        type:CONSTANTS.INTERACTION_UPDATE_SUCCESS,
    }
}

export const updateInteractionFailure = (err) =>{
    return{
        type:CONSTANTS.INTERACTION_UPDATE_FAILURE,
        message:"Unable to Update User Interactions"
    }
}


export const deleteInteractionSuccess = () =>{
    return{
        type:CONSTANTS.INTERACTION_DELETE_SUCCESS,
    }
}

export const deleteInteractionFailure = (err) =>{
    return{
        type:CONSTANTS.INTERACTION_UPDATE_FAILURE,
        message:"Unable to Delete User Interactions, Try after some time"
    }
}

export const resetInteractionErrors = () =>{
    return{
        type:CONSTANTS.INTERACTION_ERROR_RESET,
    }
}
