import axios from 'axios'
import {LOGIN_CONSTANTS as CONSTANTS} from '../constants/Constants'

const API_URL = "http://localhost:8080/api/auth/";

export const login = (authObj) =>{
    return (dispatch,getState) =>{
        dispatch(loginBegin())
         return axios.post(API_URL + "signin", authObj)
			.then(
                (response) => {
                    dispatch(loginSuccess(response.data))
                    if (response.data.accessToken) {
                        localStorage.setItem("user", JSON.stringify(response.data));
                    }},
                (error) => {
                    dispatch(loginFailure())
                    throw error
                });
}}

export const loginBegin = () =>{
    return{
        type:CONSTANTS.LOGIN_BEGIN,
    }
}

export const loginSuccess = (user) =>{
    return{
        type:CONSTANTS.LOGIN_SUCCESS,
        data:user
    }
}

export const loginFailure = (error) =>{
    return{
        type:CONSTANTS.LOGIN_FAILURE,
        message:"Invalid Data, Try after some time!"
    }
}

export const logOut = () =>{
    return{
        type:CONSTANTS.LOGOUT,
    }
}