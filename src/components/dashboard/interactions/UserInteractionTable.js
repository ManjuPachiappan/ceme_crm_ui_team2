import React from 'react'
import { deleteInteractions } from "../../store/InteractionActions";
import Loader from "../../utilities/Loader";
class Interactions extends React.Component {
    
    handleDelete = (id) => {
        const { dispatch } = this.props;
        dispatch(deleteInteractions(id))
    }

    render() {
        const DATA = this.props.interactions
        return (
            <>
                {this.props.loading ? <Loader /> :
                    <> <h2>Interaction Details <button type="button" onClick={this.props.handleAddEdit.bind(this)} className="btn btn-link">Add New</button> </h2>
                    <div className="table-responsive">
                    {this.props.message && (<div className="alert alert-danger alert-dismissible fade show">
                                    <strong>Error!</strong> {this.props.message}
                                </div>)}
                        <table className="table table-striped table-sm">
                            <thead className="thead-dark">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Customer Name</th>
                                    <th scope="col">Creation Date</th>
                                    <th scope="col">Subject</th>
                                    <th scope="col">Assigned To</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Close Date</th>
                                    <th scope="col"> </th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                {DATA.map((entry, i) => (
                                    <tr key={i}>
                                        <th scope="row">1</th>
                                        <td>{entry.custName}</td>
                                        <td>{entry.creationDate}</td>
                                        <td>{entry.subject}</td>
                                        <td>{entry.userName}</td>
                                        <td>{entry.status}</td>
                                        <td>{entry.closeDate}</td>
                                        <td>
                                            <button type="button" onClick={this.props.handleAddEdit.bind(this, entry)} className="btn btn-link">
                                                <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-pencil-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fillRule="evenodd" d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z" />
                                                </svg>
                                            </button>

                                            <button type="button" onClick={this.handleDelete.bind(this, entry.id)} className="btn btn-link">
                                                <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-trash-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fillRule="evenodd" d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5a.5.5 0 0 0-1 0v7a.5.5 0 0 0 1 0v-7z" />
                                                </svg>
                                            </button>
                                        </td>
                                    </tr>
                                ))
                                }


                            </tbody>
                        </table>
                        </div></>
                }
            </>
        )
    }
}

export default Interactions;