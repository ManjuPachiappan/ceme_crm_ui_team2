import React from 'react'
import PieChart from '../../charts/PieChart'
import { statusData } from "./DataFormatFunctions";
class TaskStatusChart extends React.Component
{

    render(){
       return  <PieChart
        data={statusData(this.props.interactions)}
        options={{
            title: 'Activity Status Report',
            is3D: true,
            colors: ['Grey','Red','Blue','Green']
        }}
        />
    }

}

export default TaskStatusChart