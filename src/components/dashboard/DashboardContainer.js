import React,{useState} from 'react'
import { connect } from 'react-redux'
import Interactions from "./interactions/UserInteractionTable";
import TaskStatusChart from "./charts/TaskStatusChart";
import InteractionForm from "./interactions/InteractionForm";

//import  PieChart  from "./charts/Canvas_PieChart";
const DashboardContainer = (props) => {
    
    const [addEditFlag, setAddEditFlag] = useState(false)
    const [interactionDetails, setInteractionDetails] = useState({})
  
    const handleAddEdit = (entry) => {
        setInteractionDetails(entry)
        setAddEditFlag(true)
    }

    const handleReset = () => {
        setInteractionDetails({})
        setAddEditFlag(false)
    }

    return (
    
        addEditFlag? <InteractionForm data={interactionDetails} callback={handleReset} /> :
            <>    
                <h1> DashBaord</h1>
                <TaskStatusChart {...props} />
                <Interactions {...props} handleAddEdit={handleAddEdit}/>
            </>
        
    )
}


const mapStateToProps = (state) => {
    const { interactions, loading, message } = state.interaction;
    return {
        interactions,
        loading,
        message
    };
}

export default connect(mapStateToProps)(DashboardContainer);



